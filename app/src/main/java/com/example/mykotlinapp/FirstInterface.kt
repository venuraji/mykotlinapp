package com.example.mykotlinapp

interface FirstInterface {
    fun firstFunction():Boolean
    fun secondFunction(): String
}