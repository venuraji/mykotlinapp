package com.example.mykotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity(),FirstInterface ,SecondInterface{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun firstFunction(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun secondFunction(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun firstFunctionBySecond(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
